#!/bin/bash

sleep 5

flask db upgrade
gunicorn --bind 0.0.0.0:8000 --reload wsgi:app
