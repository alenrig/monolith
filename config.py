import os


class Config(object):

    # APP
    SECRET_KEY = os.environ.get("SECRET_KEY")
    SECURITY_PASSWORD_SALT = os.environ.get("SECURITY_PASSWORD_SALT")

    # ADMIN
    BASIC_AUTH_USERNAME = os.environ.get('BASIC_AUTH_USERNAME')
    BASIC_AUTH_PASSWORD = os.environ.get('BASIC_AUTH_PASSWORD')

    # TOKEN
    TOKEN_EXPIRATION_TIME = 600

    # LOGS
    LOG_TO_STDOUT = os.environ.get("LOG_TO_STDOUT")

    # DB
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or "null"
    if SQLALCHEMY_DATABASE_URI.startswith("postgres://"):
        SQLALCHEMY_DATABASE_URI = SQLALCHEMY_DATABASE_URI.replace("postgres://", "postgresql://", 1)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Celery
    CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL")
    CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND")

    # MAIL
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = os.environ.get("MAIL_PORT")
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS")
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = os.environ.get("MAIL_DEFAULT_SENDER")


class TestConfig(Config):
    TESTING = True

    SERVER_NAME = "homeland.dev"

    BASIC_AUTH_USERNAME = "asd"
    BASIC_AUTH_PASSWORD = "123"

    SECRET_KEY = "123"
    SECURITY_PASSWORD_SALT = "456"

    SQLALCHEMY_DATABASE_URI = "sqlite:///"
    BCRYPT_LOG_ROUNDS = 4
    WTF_CSRF_ENABLED = False

    MAIL_DEFAULT_SENDER = "admin@homeland.py"
    MAIL_SUPPRESS_SEND = True
