"""добавил столбец подтвержденной почты

Revision ID: db2cc5bead37
Revises: 2416093e2ec6
Create Date: 2020-04-04 09:08:18.706500

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'db2cc5bead37'
down_revision = '2416093e2ec6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('confirmed_email', sa.Boolean(), server_default='f', nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'confirmed_email')
    # ### end Alembic commands ###
