from flask_mail import Message

from . import celery, mail


@celery.task
def send_async_email(message: Message) -> None:
    mail.send(message)
