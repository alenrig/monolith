from hashlib import md5

from flask_login import UserMixin

from app import db, login


class User(UserMixin, db.Model):  # type: ignore
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    confirmed_email = db.Column(
        db.Boolean, nullable=False, default=False, server_default="f"
    )
    about_me = db.Column(db.String(140))

    def avatar(self, size: int) -> str:
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        return f"https://www.gravatar.com/avatar/{digest}?d=identicon&s={size}"


@login.user_loader
def load_user(id: int) -> int:
    return User.query.get(int(id))
