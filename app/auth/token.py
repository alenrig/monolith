from typing import Union

from flask import current_app
from itsdangerous import URLSafeTimedSerializer


def generate_token(email: str) -> Union[str, bytes]:
    serializer = URLSafeTimedSerializer(current_app.config["SECRET_KEY"])
    return serializer.dumps(email, current_app.config["SECURITY_PASSWORD_SALT"])


def confirm_token(token: str) -> str:
    serializer = URLSafeTimedSerializer(current_app.config["SECRET_KEY"])
    email = serializer.loads(
        token,
        salt=current_app.config["SECURITY_PASSWORD_SALT"],
        max_age=current_app.config["TOKEN_EXPIRATION_TIME"],
    )
    return email
