from typing import Callable, Union

from app import db
from app.auth import bp
from app.auth.forms import LoginForm, RegistrationForm
from app.auth.token import confirm_token
from app.models import User
from app.tasks import send_async_email
from app.utils import ConfirmEmailMessage
from flask import flash, redirect, render_template, url_for
from flask_login import current_user, login_user, logout_user
from itsdangerous import SignatureExpired


@bp.route("/login", methods=["GET", "POST"])
def login() -> Union[Callable, str]:
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or user.email != form.email.data:
            flash("Invalid username or email.", "danger")
            return redirect(url_for("auth.login"))
        if not user.confirmed_email:
            flash("Please check your email to complete registration.", "danger")
            return redirect(url_for("auth.login"))
        login_user(user, remember=form.remember_me.data)

        return redirect(url_for("main.index"))
    return render_template("auth/login.html", title="Sign In", form=form)


@bp.route("/logout")
def logout() -> Callable:
    logout_user()
    return redirect(url_for("main.index"))


@bp.route("/register", methods=["GET", "POST"])
def register() -> Union[Callable, str]:
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        db.session.add(user)
        db.session.commit()
        message = ConfirmEmailMessage(form.email.data)
        send_async_email(message)

        flash("Your registration is successful! Please check your email.")
        return redirect(url_for("auth.login"))

    return render_template("auth/register.html", title="Register", form=form)


@bp.route("/confirm/<token>")
def confirm_url(token: str) -> Callable:
    try:
        email = confirm_token(token)
    except SignatureExpired:
        flash("The confirmation link is invalid of has expired.", "danger")
        return redirect(url_for("auth.register"))
    user = User.query.filter_by(email=email).first_or_404()
    if user.confirmed_email:
        flash("Account already confirmed. Please login.", "success")
    else:
        user.confirmed_email = True
        db.session.add(user)
        db.session.commit()
        flash("You have confirmed your account. Thanks!", "success")
    return redirect(url_for("auth.login"))
