from typing import Callable, Union

from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required

from app import db
from app.main import bp
from app.main.forms import EditProfileForm, SettingsForm
from app.models import User


@bp.route("/", methods=["GET", "POST"])
@bp.route("/index", methods=["GET", "POST"])
@login_required
def index() -> str:
    return render_template("index.html")


@bp.route("/user/<username>")
@login_required
def user(username: str) -> str:
    user = User.query.filter_by(username=username).first_or_404()
    return render_template("user.html", user=user)


@bp.route("/edit_profile", methods=["GET", "POST"])
@login_required
def edit_profile() -> Union[str, Callable]:
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash("Your changes have been saved.")
        return redirect(url_for("main.user", username=current_user.username))
    elif request.method == "GET":
        form.about_me.data == current_user.about_me
    return render_template("edit_profile.html", title="Edit Profile", form=form)


@bp.route("/settings", methods=["GET", "POST"])
@login_required
def settings() -> str:
    form = SettingsForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        db.session.commit()
        flash("Your changes have been saved.")
    elif request.method == "GET":
        form.username.data == current_user.username
    return render_template("settings.html", title="Settings", form=form)
