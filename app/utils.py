from flask import current_app
from flask import url_for
from flask import render_template
from app.auth.token import generate_token
from flask_mail import Message


class ConfirmEmailMessage(Message):
    """Custom class for email confirmation messages.

    Args:
        Message ([class]): Basic class from Flask-Mail module.
    """

    def __init__(self, recipient: str) -> None:
        super().__init__(
            subject="Confirm Email",
            recipients=[recipient],
            html=self.create_message_body(recipient),
            sender=current_app.config["MAIL_DEFAULT_SENDER"],
        )
        self.recipient = recipient

    def create_message_body(self, email: str) -> str:
        token = generate_token(email)
        return render_template(
            "auth/confirm_token_email.html",
            confirm_url=url_for("auth.confirm_url", token=token, _external=True),
        )
