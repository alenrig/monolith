from flask import redirect
from flask_admin.contrib import sqla
from werkzeug import Response
from werkzeug.exceptions import HTTPException

from app import admin, basic_auth, db
from app.models import User


class AuthException(HTTPException):
    def __init__(self, message) -> None:
        super().__init__(
            message,
            Response(
                "You could not be authenticated. Please refresh the page.",
                401,
                {"WWW-Authenticate": 'Basic realm="Login Required"'},
            ),
        )


class ModelView(sqla.ModelView):
    def is_accessible(self) -> bool:
        if not basic_auth.authenticate():
            raise AuthException("Not authenticated")
        else:
            return True

    def inaccessible_callback(self, name, **kwargs) -> Response:
        return redirect(basic_auth.challenge())


def create_admin() -> None:
    admin.add_view(ModelView(User, db.session))
