import logging

from celery import Celery
from flask import Flask
from flask_admin import Admin
from flask_basicauth import BasicAuth
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config

admin = Admin(name="Homeland-Admin", template_mode="bootstrap3")
basic_auth = BasicAuth()
celery = Celery()
bootstrap = Bootstrap()
db = SQLAlchemy()
login = LoginManager()
login.login_view = "auth.login"
mail = Mail()
migrate = Migrate()


def create_app(config_class=Config) -> Flask:
    app = Flask(__name__)
    app.config.from_object(config_class)

    celery.main = app.name
    celery.conf.update(app.config)

    admin.init_app(app)
    basic_auth.init_app(app)
    bootstrap.init_app(app)
    db.init_app(app)
    login.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)

    from app.auth import bp as auth_bp
    from app.errors import bp as errors_bp
    from app.main import bp as main_bp

    app.register_blueprint(auth_bp, url_prefix="/auth")
    app.register_blueprint(errors_bp)
    app.register_blueprint(main_bp)

    if not app.debug:  # pragma: no cover
        if app.config["LOG_TO_STDOUT"]:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.WARN)
            app.logger.addHandler(stream_handler)

    return app


from app import models  # noqa F401
