FROM python:3.9

WORKDIR /app

RUN pip install --no-cache pipenv==2018.11.26

COPY Pipfile* ./

RUN pipenv install --deploy --system --clear

COPY . .

ENTRYPOINT [ "./boot.sh" ]
