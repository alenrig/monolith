from app import create_app, db
from app.admn import create_admin
from app.models import User


app = create_app()
create_admin()


@app.shell_context_processor
def make_shell_context():
    return {"db": db, "User": User}
