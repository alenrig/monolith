import pytest

from app import create_app, db
from app.models import User
from config import TestConfig

USERS = [
    {
        "id": 1,
        "username": "TestUser",
        "email": "user@mail.com",
        "confirmed_email": True,
    },
    {
        "id": 2,
        "username": "TestUser2",
        "email": "user2@mail.com",
        "confirmed_email": True,
    },
    {
        "id": 3,
        "username": "TestUser3",
        "email": "user3@mail.com",
        "confirmed_email": False,
    },
]


def _create_db():
    db.create_all()
    for i in USERS:
        user = User(
            username=i["username"],
            email=i["email"],
            confirmed_email=i["confirmed_email"],
        )
        db.session.add(user)
    db.session.commit()


@pytest.fixture()
def test_app():
    app = create_app(config_class=TestConfig)
    with app.app_context():
        yield app


@pytest.fixture()
def test_db():
    _create_db()
    yield db
    db.drop_all()


@pytest.fixture()
def test_client():
    app = create_app(config_class=TestConfig)
    with app.app_context():
        yield app.test_client()


@pytest.fixture(scope="class")
def test_client_authorized():
    app = create_app(config_class=TestConfig)
    with app.app_context():
        _create_db()
        data = USERS[0]
        client = app.test_client()
        client.post("/auth/login", data=data, follow_redirects=True)
        yield client
        db.drop_all()
