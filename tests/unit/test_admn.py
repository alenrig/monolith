import pytest


@pytest.mark.usefixtures("test_client")
class TestAdmin:
    def test_main_page(self, test_client):
        response = test_client.get("/admin", follow_redirects=True)
        assert response.status_code == 200
        assert b"Homeland-Admin" in response.data
