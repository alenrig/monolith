import pytest


@pytest.mark.usefixtures("test_client", "test_db")
class TestHandlers:
    def test_not_found_error(self, test_client):
        response = test_client.get("/main/test_wtf")
        assert response.status_code == 404
        assert b"File Not Found" in response.data
