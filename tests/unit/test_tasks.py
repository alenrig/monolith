import pytest
from app import mail
from app.tasks import send_async_email
from app.utils import ConfirmEmailMessage


@pytest.mark.usefixtures("test_app", "test_db")
class TestEmail:
    def test_send_email(self, test_app):
        with mail.record_messages() as outbox:
            message = ConfirmEmailMessage("test@mail.com")
            send_async_email(message)
            assert len(outbox) == 1
            assert outbox[0].subject == "Confirm Email"
