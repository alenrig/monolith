import pytest
from tests.conftest import USERS


@pytest.mark.usefixtures("test_client_authorized")
class TestRoutesAuthorized:
    def test_main_page(self, test_client_authorized):
        response = test_client_authorized.get("/")
        assert response.status_code == 200
        assert f"Welcome, {USERS[0]['username']}!".encode() in response.data

    @pytest.mark.parametrize(
        "user, expected_response_code, expected_data",
        [
            (USERS[0]["username"], 200, f"User: {USERS[0]['username']}".encode()),
            ("Non_Existed_User", 404, b"File Not Found"),
        ],
    )
    def test_user_page(
        self, test_client_authorized, user, expected_response_code, expected_data
    ):
        response = test_client_authorized.get(f"/user/{user}", follow_redirects=True)
        assert response.status_code == expected_response_code
        assert expected_data in response.data

    def test_edit_profile_page(self, test_client_authorized):
        response = test_client_authorized.get("/edit_profile")
        assert response.status_code == 200
        assert b"Edit Profile" in response.data

    def test_settings_page(self, test_client_authorized):
        response = test_client_authorized.get("/settings", follow_redirects=True)
        assert response.status_code == 200
        assert b"Settings" in response.data


@pytest.mark.usefixtures("test_client_authorized")
class TestForms(object):
    def test_edit_profile(self, test_client_authorized):
        data = {"about_me": "very boring story"}
        response = test_client_authorized.post(
            "/edit_profile", data=data, follow_redirects=True
        )
        assert response.status_code == 200
        assert f"User: {USERS[0]['username']}".encode() in response.data

    @pytest.mark.parametrize(
        "username, expected_response_code, expected_data",
        [
            (USERS[0]["username"], 200, b"Settings"),
            (USERS[1]["username"], 200, b"Please use a different username"),
        ],
    )
    def test_edit_username(
        self, test_client_authorized, username, expected_response_code, expected_data
    ):
        data = {"username": username}
        response = test_client_authorized.post(
            "/settings", data=data, follow_redirects=True
        )
        assert response.status_code == expected_response_code
        assert expected_data in response.data
