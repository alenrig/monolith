from html.parser import HTMLParser
from typing import List

import pytest
from itsdangerous import SignatureExpired

from app import mail
from app.auth import token
from tests.conftest import USERS


class Parser(HTMLParser):
    links: List[str] = []

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            self.links = [value for name, value in attrs if name == "href"]


@pytest.mark.usefixtures("test_client", "test_db")
class TestRoutes:
    def test_auth_page(self, test_client):
        response = test_client.get("/auth/login")
        assert response.status_code == 200
        assert b"Sign In" in response.data

    @pytest.mark.parametrize(
        "data, expected",
        [
            (USERS[0], f"Welcome, {USERS[0]['username']}!".encode()),
            (
                {"username": USERS[0]["username"], "email": "bad@mail.com"},
                b"Invalid username or email",
            ),
            (
                {"username": "NewUser", "email": USERS[0]["email"]},
                b"Invalid username or email",
            ),
        ],
    )
    def test_login(self, test_client, data, expected):
        response = test_client.post("/auth/login", data=data, follow_redirects=True)
        assert expected in response.data

    def test_login_unconfirmed_email(self, test_client, test_db):
        data = USERS[2]
        response = test_client.post("auth/login", data=data, follow_redirects=True)
        assert response.status_code == 200
        assert b"Please check your email to complete registration." in response.data

    def test_register_page(self, test_client):
        response = test_client.get("/auth/register")
        assert response.status_code == 200
        assert b"Register" in response.data

    def test_registration(self, test_client):
        data = {"username": "NewUser", "email": "new@mail.com"}
        response = test_client.post("/auth/register", data=data, follow_redirects=True)
        assert response.status_code == 200
        assert b"Sign In" in response.data
        assert b"Your registration is successful! Please check your email." in response.data

    def _registration(self, test_client):
        data = {"username": "NewUser", "email": "new@mail.com"}
        with mail.record_messages() as outbox:
            _ = test_client.post("/auth/register", data=data, follow_redirects=True)
            message = outbox[0].html
        parser = Parser()
        parser.feed(message)
        confirmation_link = parser.links[0]
        token = confirmation_link.split("/")[-1]
        return token

    def test_email_confirmation(self, test_client):
        token = self._registration(test_client)
        response = test_client.get(f"/auth/confirm/{token}", follow_redirects=True)
        assert response.status_code == 200
        assert b"You have confirmed your account. Thanks!" in response.data

    def test_confirm_token_again(self, test_client):
        token = self._registration(test_client)
        response = test_client.get(f"/auth/confirm/{token}", follow_redirects=True)
        response = test_client.get(f"/auth/confirm/{token}", follow_redirects=True)
        assert response.status_code == 200
        assert b"Account already confirmed. Please login." in response.data

    def test_invalid_token(self, test_client):
        token = self._registration(test_client)
        test_client.application.config["TOKEN_EXPIRATION_TIME"] = -1
        response = test_client.get(f"/auth/confirm/{token}", follow_redirects=True)
        assert response.status_code == 200
        assert b"The confirmation link is invalid of has expired." in response.data

    def test_registration_validation(self, test_client, test_db):
        data = USERS[0]
        response = test_client.post("/auth/register", data=data)
        assert b"Please use a different username." in response.data
        assert b"Please use a different email address." in response.data


@pytest.mark.usefixtures("test_client_authorized")
class TestRoutesAuthorized:
    def test_login_page_redirect(self, test_client_authorized):
        response = test_client_authorized.get("/auth/login")
        assert response.status_code == 302

    def test_register_page_redirect(self, test_client_authorized):
        response = test_client_authorized.get("/auth/register")
        assert response.status_code == 302

    def test_logout(self, test_client_authorized):
        response = test_client_authorized.get("/auth/logout", follow_redirects=True)
        assert response.status_code == 200
        assert b"Sign In" in response.data


@pytest.mark.usefixtures("test_app")
class TestToken:
    def test_confirm_token(self):
        email = "test@mail.com"
        generated_token = token.generate_token(email)
        unencrypted_token = token.confirm_token(generated_token)
        assert unencrypted_token == email

    def test_token_expiration(self, test_app):
        email = "test@mail.com"
        test_app.config["TOKEN_EXPIRATION_TIME"] = -1
        generated_token = token.generate_token(email)
        with pytest.raises(SignatureExpired):
            _ = token.confirm_token(generated_token)
