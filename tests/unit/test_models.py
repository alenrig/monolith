import pytest

from app.models import User


@pytest.mark.usefixtures("test_app", "test_db")
class TestUser:
    def test_avatar(self):
        user = User(username="test", email="test@mail.com")
        expected = "https://www.gravatar.com/avatar/97dfebf4098c0f5c16bca61e2b76c373?d=identicon&s=128"  # noqa: E501
        assert user.avatar(128) == expected
